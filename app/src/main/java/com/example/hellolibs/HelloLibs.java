package com.example.hellolibs;

public class HelloLibs {
    static {
        System.loadLibrary("hello-libs");
    }

    public native String stringFromJNI();
}
